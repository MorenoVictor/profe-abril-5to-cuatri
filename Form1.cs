﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Windows.Forms;
using System.IO;


namespace ExploradorDeSqlite
{
    
    public partial class Form1 : Form
    {
        String[] basesd;
        private string directorio;
        private string bd;
        private SQLiteConnection con;
        private SQLiteCommand cmd;
        private SQLiteDataAdapter DA;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        private SQLiteDataReader SEL;


        public Form1()
        {
            InitializeComponent();
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {

            comboBox1.Items.Clear();

            try
            {
                folder.ShowDialog();
                label1.Text = "Folder actual  " + folder.SelectedPath;
                basesd = Directory.GetFiles(folder.SelectedPath,"*.db");

               

                comboBox1.Items.Add("Seleccione la bd para mostrar");
               
                comboBox1.Text = comboBox1.Items[0].ToString();
                directorio = folder.SelectedPath;
                foreach (var item in basesd)
                {
                    comboBox1.Items.Add(Path.GetFileName(item));
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Fallo la ruta ");
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Add("Mostrar bd");
            //comboBox1.Items.Clear();
            comboBox1.Text = comboBox1.Items[0].ToString();
        }

       

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            label3.Text = "Tabla de la bd " + comboBox1.Text.ToString();
            bd = comboBox1.Text.ToString();

            comboMostrar.Items.Clear();

            try
            {
               // MessageBox.Show(directorio+ "\\" + bd);
                con = new SQLiteConnection("Data Source=" + directorio+ "\\"+ bd + ";");
                con.Open();
                cmd = new SQLiteCommand("SELECT name FROM sqlite_master WHERE type = \"table\";", con);
                //cmd.CommandText = "SELECT name FROM "+bd+".sqlite_master WHERE type='table';";
                SEL = cmd.ExecuteReader();

                if (SEL.HasRows)
                {
                    comboMostrar.Enabled = true;

                    while (SEL.Read())
                    {
                        comboMostrar.Items.Add(SEL.GetString(0));
                       
                    }

                    comboMostrar.Text = comboMostrar.Items[0].ToString();

                }

               

                con.Close();

                //cmd.CommandText = query;

            }
            catch (Exception error)
            {

                MessageBox.Show("Error al seleccionar las tablas"+error);
            }

        }

        private void label3_Click(object sender, EventArgs e)
        {
      
            label3.Text = "Base de datos actual  " + folder.SelectedPath;
            basesd = Directory.GetFiles(folder.SelectedPath, "*.db");
            directorio = folder.SelectedPath;
        }

        private void btnCargar_Click(object sender, EventArgs e)
        {
            String Sqlquery = "SELECT * FROM " + comboMostrar.Text;

            try
            {
                // MessageBox.Show(directorio+ "\\" + bd);
                con = new SQLiteConnection("Data Source=" + directorio + "\\" + bd + ";");
                con.Open();
                cmd = new SQLiteCommand(Sqlquery , con);
                //cmd.CommandText = "SELECT name FROM "+bd+".sqlite_master WHERE type='table';";
                cmd.ExecuteNonQuery();

                SQLiteDataAdapter tablaqsl = new SQLiteDataAdapter(cmd);

                DataTable tablaMostrar = new DataTable();

                tablaqsl.Fill(tablaMostrar);

                dgvMostrar.DataSource = tablaMostrar;


                //Cragamos tab

                con.Close();

                //cmd.CommandText = query;

            }
            catch (Exception error)
            {

                MessageBox.Show("Error al seleccionar los datos de la tabla" + error);
            }


            //MessageBox.Show(Sqlquery);
        }
    }
}
